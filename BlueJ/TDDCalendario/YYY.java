
import java.time.DayOfWeek;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * The test class YYY.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class YYY
{
    /**
     * Default constructor for test class YYY
     */
    public YYY()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @BeforeEach
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @AfterEach
    public void tearDown()
    {
    }
    
    @Test
    public void aSaturdayIsHoliday(){
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        LocalDate aSaturday = LocalDate.of(2024, 4, 20);
        holidayCalendar.addHolidayRule(new DayOfWeekHolidayRule(DayOfWeek.SATURDAY));
        assertTrue(holidayCalendar.isHoliday(aSaturday));
    }

    @Test
    public void aMondayIsNotHoliday(){
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        LocalDate aMonday = LocalDate.of(2024, 4, 15);
        assertFalse(holidayCalendar.isHoliday(aMonday));
    }

    @Test
    public void aSundayIsHoliday(){
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        LocalDate aSunday = LocalDate.of(2024, 4, 14);
        holidayCalendar.addHolidayRule(new DayOfWeekHolidayRule(DayOfWeek.SUNDAY));
        assertTrue(holidayCalendar.isHoliday(aSunday));
    }

    @Test
    public void aSaturdayAndSundayIsHoliday(){
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        LocalDate aSaturday = LocalDate.of(2024, 4, 13);
        LocalDate aSunday = LocalDate.of(2024, 4, 14);
        holidayCalendar.addHolidayRule(new DayOfWeekHolidayRule(DayOfWeek.SATURDAY));
        holidayCalendar.addHolidayRule(new DayOfWeekHolidayRule(DayOfWeek.SUNDAY));
        assertTrue(holidayCalendar.isHoliday(aSaturday));
        assertTrue(holidayCalendar.isHoliday(aSunday));
    }

    @Test
    public void aJunaryFirstIsHoliday() {
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        holidayCalendar.addHolidayRule(new DayOfMonthHolidayRule(1,1));
        LocalDate aJunaryFirst = LocalDate.of(2024, 1, 1);
        assertTrue(holidayCalendar.isHoliday(aJunaryFirst));
    }

    @Test
    public void aAprilFirstIsHoliday() {
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        holidayCalendar.addHolidayRule(new DayOfMonthHolidayRule(1, 1));
        LocalDate aAprilFirst = LocalDate.of(2024, 4, 1);
        assertFalse(holidayCalendar.isHoliday(aAprilFirst));
    }

    @Test
    public void aJunaryFirstAndNavidadIsHoliday() {
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        holidayCalendar.addHolidayRule(new DayOfMonthHolidayRule(1, 1));
        holidayCalendar.addHolidayRule(new DayOfMonthHolidayRule(12,25));
        LocalDate aNavidadDate = LocalDate.of(2024, 12, 25);
        LocalDate aJunaryFirst = LocalDate.of(2024, 1, 1);
        assertTrue(holidayCalendar.isHoliday(aNavidadDate));
        assertTrue(holidayCalendar.isHoliday(aJunaryFirst));
    }

    @Test
    public void aDateIsHoliday() {
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        LocalDate aDate = LocalDate.of(2024, 5, 1);
        holidayCalendar.addHolidayRule(new DateHolidayRule(aDate));
        assertTrue(holidayCalendar.isHoliday(aDate));
    }
    
    @Test
    public void mondayIsHolidayInRange1990To1999() {
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        holidayCalendar.addHolidayRule(new CompoundRangeHolidayRule(LocalDate.of(1990, 1, 1), LocalDate.of(1999, 12, 31), new DayOfWeekHolidayRule(DayOfWeek.MONDAY)));
        assertTrue(holidayCalendar.isHoliday(LocalDate.of(1998, 3, 2)));
    }
}