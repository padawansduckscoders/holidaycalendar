import java.time.DayOfWeek;
import java.time.LocalDate;

public class DayOfWeekHolidayRule implements HolidayRule {

    private DayOfWeek dayOfWeek;

    public DayOfWeekHolidayRule(DayOfWeek DayOfWeek) {
        this.dayOfWeek = DayOfWeek;
    }

    @Override
    public boolean isHoliday(LocalDate aDate) {
        return aDate.getDayOfWeek().equals(dayOfWeek);
    }
}
