import java.time.LocalDate;

interface HolidayRule {
    boolean isHoliday(LocalDate aDate);
}