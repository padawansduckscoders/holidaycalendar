package src.main.java;

import java.time.LocalDate;
import java.util.ArrayList;

public class HolidayCalendar {

    private ArrayList<HolidayRule> holidayRules = new ArrayList<HolidayRule>();

    public boolean isHoliday(LocalDate aDate) {
        return holidayRules.stream().anyMatch(aHolidayRule -> aHolidayRule.isHoliday(aDate));
    }

    public void addHolidayRule(HolidayRule holidayRule) {
        holidayRules.add(holidayRule);
    }
}