package src.test.java;

import java.time.DayOfWeek;
import java.time.LocalDate;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import src.main.java.DateHolidayRule;
import src.main.java.DayOfMonthHolidayRule;
import src.main.java.DayOfWeekHolidayRule;
import src.main.java.CompoundRangeHolidayRule;
import src.main.java.HolidayCalendar;

public class HolidayCalendarTest {
    @Test
    public void aSaturdayIsHoliday(){
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        LocalDate aSaturday = LocalDate.of(2024, 4, 20);
        holidayCalendar.addHolidayRule(new DayOfWeekHolidayRule(DayOfWeek.SATURDAY));
        Assert.assertTrue(holidayCalendar.isHoliday(aSaturday));
    }

    @Test
    public void aMondayIsNotHoliday(){
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        LocalDate aMonday = LocalDate.of(2024, 4, 15);
        Assert.assertFalse(holidayCalendar.isHoliday(aMonday));
    }

    @Test
    public void aSundayIsHoliday(){
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        LocalDate aSunday = LocalDate.of(2024, 4, 14);
        holidayCalendar.addHolidayRule(new DayOfWeekHolidayRule(DayOfWeek.SUNDAY));
        Assert.assertTrue(holidayCalendar.isHoliday(aSunday));
    }

    @Test
    public void aSaturdayAndSundayIsHoliday(){
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        LocalDate aSaturday = LocalDate.of(2024, 4, 13);
        LocalDate aSunday = LocalDate.of(2024, 4, 14);
        holidayCalendar.addHolidayRule(new DayOfWeekHolidayRule(DayOfWeek.SATURDAY));
        holidayCalendar.addHolidayRule(new DayOfWeekHolidayRule(DayOfWeek.SUNDAY));
        Assert.assertTrue(holidayCalendar.isHoliday(aSaturday));
        Assert.assertTrue(holidayCalendar.isHoliday(aSunday));
    }

    @Test
    public void aJunaryFirstIsHoliday() {
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        holidayCalendar.addHolidayRule(new DayOfMonthHolidayRule(1,1));
        LocalDate aJunaryFirst = LocalDate.of(2024, 1, 1);
        Assert.assertTrue(holidayCalendar.isHoliday(aJunaryFirst));
    }

    @Test
    public void aAprilFirstIsHoliday() {
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        holidayCalendar.addHolidayRule(new DayOfMonthHolidayRule(1, 1));
        LocalDate aAprilFirst = LocalDate.of(2024, 4, 1);
        Assert.assertFalse(holidayCalendar.isHoliday(aAprilFirst));
    }

    @Test
    public void aJunaryFirstAndNavidadIsHoliday() {
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        holidayCalendar.addHolidayRule(new DayOfMonthHolidayRule(1, 1));
        holidayCalendar.addHolidayRule(new DayOfMonthHolidayRule(12,25));
        LocalDate aNavidadDate = LocalDate.of(2024, 12, 25);
        LocalDate aJunaryFirst = LocalDate.of(2024, 1, 1);
        Assert.assertTrue(holidayCalendar.isHoliday(aNavidadDate));
        Assert.assertTrue(holidayCalendar.isHoliday(aJunaryFirst));
    }

    @Test
    public void aDateIsHoliday() {
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        LocalDate aDate = LocalDate.of(2024, 5, 1);
        holidayCalendar.addHolidayRule(new DateHolidayRule(aDate));
        Assert.assertTrue(holidayCalendar.isHoliday(aDate));
    }
    
    @Test
    public void mondayIsHolidayInRange1990To1999() {
        HolidayCalendar holidayCalendar = new HolidayCalendar();
        holidayCalendar.addHolidayRule(new CompoundRangeHolidayRule(LocalDate.of(1990, 1, 1), LocalDate.of(1999, 12, 31), new DayOfWeekHolidayRule(DayOfWeek.MONDAY)));
        Assert.assertTrue(holidayCalendar.isHoliday(LocalDate.of(1998, 3, 2)));
    }
}
